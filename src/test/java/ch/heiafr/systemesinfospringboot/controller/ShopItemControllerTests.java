package ch.heiafr.systemesinfospringboot.controller;/*
 * ---------------------------------------------------------
 * Project: systemes-info-springboot
 * Author: Loïck Roth
 * Date: 17.10.2022
 * Version: 0.1
 * Email: loick.roth@edu.hefr.ch
 * ---------------------------------------------------------
 */

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
class ShopItemControllerTests {

    @Autowired
    private ShopItemController ctrl;

    @Test
    void contextLoads() throws Exception {
        assertThat(ctrl).isNotNull();
    }
}
