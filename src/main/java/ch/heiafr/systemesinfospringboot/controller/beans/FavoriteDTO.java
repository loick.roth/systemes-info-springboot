package ch.heiafr.systemesinfospringboot.controller.beans;/*
 * ---------------------------------------------------------
 * Project: systemes-info-springboot
 * Author: Loïck Roth
 * Date: 30.10.2022
 * Version: 0.1
 * Email: loick.roth@edu.hefr.ch
 * ---------------------------------------------------------
 */

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FavoriteDTO {

    private boolean favorite;

}
