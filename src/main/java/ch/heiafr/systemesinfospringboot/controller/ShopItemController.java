package ch.heiafr.systemesinfospringboot.controller;

/*
 * ---------------------------------------------------------
 * Project: systemes-info-springboot
 * Author: Loïck Roth
 * Date: 05.10.2022
 * Version: 0.1
 * Email: loick.roth@edu.hefr.ch
 * ---------------------------------------------------------
 */

import ch.heiafr.systemesinfospringboot.controller.beans.FavoriteDTO;
import ch.heiafr.systemesinfospringboot.entity.ShopItem;
import ch.heiafr.systemesinfospringboot.service.ShopItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shopitems")
public class ShopItemController {

    private static final Logger log = LoggerFactory.getLogger(ShopItemController.class);

    @Autowired
    private ShopItemService shopService;

    @PostMapping
    public void postItem(@RequestBody ShopItem item) {
        shopService.saveItem(item);
    }

    @DeleteMapping("/favorites")
    public void removeFavorites() {
        List<ShopItem> favorites = getFavorites();
        for (ShopItem i : favorites) {
            i.setFavorite(false);
        }
        shopService.saveAllItems(favorites);
    }

    @GetMapping("/favorites")
    public List<ShopItem> getFavorites() {
        List<ShopItem> result = getItems();
        result.removeIf(item -> !item.getFavorite());
        return result;
    }

    @GetMapping
    public @ResponseBody List<ShopItem> getItems() {
        return shopService.getItems();
    }

    @PutMapping("/{id}/favorite")
    public void setFavorite(@PathVariable(value = "id") int id, @RequestBody FavoriteDTO payload) {
        log.info("favorite to {0} for id {1}", payload.isFavorite(), id);
        Optional<ShopItem> optItem = shopService.getItemById(id);
        if (optItem.isPresent()) {
            ShopItem item = optItem.get();
            item.setFavorite(payload.isFavorite());
            shopService.saveItem(item);
        } else {
            log.info("entity with id {0} not found", id);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "entity with id {" + id + "} not found"
            );
        }

    }

}
