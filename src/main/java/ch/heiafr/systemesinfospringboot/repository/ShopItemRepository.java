package ch.heiafr.systemesinfospringboot.repository;

/*
 * ---------------------------------------------------------
 * Project: systemes-info-springboot
 * Author: Loïck Roth
 * Date: 05.10.2022
 * Version: 0.1
 * Email: loick.roth@edu.hefr.ch
 * ---------------------------------------------------------
 */

import ch.heiafr.systemesinfospringboot.entity.ShopItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopItemRepository extends JpaRepository<ShopItem, Integer> {
}