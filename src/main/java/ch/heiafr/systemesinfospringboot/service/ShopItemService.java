package ch.heiafr.systemesinfospringboot.service;

/*
 * ---------------------------------------------------------
 * Project: systemes-info-springboot
 * Author: Loïck Roth
 * Date: 05.10.2022
 * Version: 0.1
 * Email: loick.roth@edu.hefr.ch
 * ---------------------------------------------------------
 */

import ch.heiafr.systemesinfospringboot.entity.ShopItem;
import ch.heiafr.systemesinfospringboot.repository.ShopItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShopItemService {

    @Autowired
    private ShopItemRepository repository;

    public List<ShopItem> getItems() {
        return repository.findAll();
    }

    public Optional<ShopItem> getItemById(Integer id) {
        return repository.findById(id);
    }

    public ShopItem saveItem(ShopItem shopItem) {
        return repository.save(shopItem);
    }

    public List<ShopItem> saveAllItems(List<ShopItem> items) {
        return repository.saveAll(items);
    }

}
