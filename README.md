# SI Demo project for SpringBoot
Demo project in SpringBoot 2.7.4 and Java 17.

## Build and test the project
Configure the right _spring.datasource.url_ in _application.properties_ first
````
docker-compose up -d mysql &&
./mvnw clean verify
````

## Build and deploy into the docker container
Configure the right _spring.datasource.url_ in _application.properties_ first
````
./mvnw clean package -DskipTests &&
docker-compose build &&
docker-compose up -d
````

## Run outside the docker container
Configure the right _spring.datasource.url_ in _application.properties_ first
- Use your IDE and run the Application class
- _or_ use:
````
docker-compose up -d mysql &&
java -jar target/systemes-info-springboot-*-SNAPSHOT.jar
````